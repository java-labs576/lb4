package ua.nure.akimenko.lb4;

import java.io.IOException;
import javax.servlet.jsp.tagext.TagSupport;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class InsertContentTag extends TagSupport {
    private String path = "";

    public void setPath(String path) {
        this.path = path;
    }

    public int doStartTag() {
        Path file_path = Paths.get(path);
        if (!Files.exists(file_path)) {
            System.out.println("### File does not exist");
        }
        try {
            List<String> lines = Files.readAllLines(file_path);
            for (String line : lines) {
                pageContext.getOut().println(line + "<br>");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return EVAL_BODY_INCLUDE;
    }
}
